'''
Delete all directories above a certain size in a given path

Written by: Roy Peer
'''

import os
import shutil
import argparse
from pathlib import Path

MAX_ALLOWED_SIZE_BYTES = 2
ABS_DELETE_PATH = "C:/Users/User/Desktop/DeleletHere"

'''
Recursively sum all sizes of files inside a given directory

Return: size in Bytes
'''
def get_folder_recursive_size(folder_path):
    total_size = os.path.getsize(folder_path)
    for item in os.scandir(folder_path):
        item_path = os.path.join(folder_path, item.name)
        if os.path.isfile(item_path):
            total_size += os.path.getsize(item_path)
        elif os.path.isdir(item_path):
            total_size += get_folder_recursive_size(item_path)
    return total_size

'''
Start from a given path and delete all oversized directories (top_down)
'''
def delete_oversized_directories_top_down(folder_path, max_size, ignore_errors = True):
    for item in os.scandir(folder_path):
        item_path = os.path.join(folder_path, item.name)

        if os.path.isdir(item_path):
            if get_folder_recursive_size(item_path) > max_size:
                print("Deleting " + item_path)
                shutil.rmtree(item_path, ignore_errors)

'''
Start from a given path and delete all oversized directories (bottom_up)
NOTICE: This is O(N^2). A more efficient O(N) implementation is possible
'''
def delete_oversized_directories_bottom_up(folder_path, max_size, ignore_errors = True):
    for root, dirs, files in list(os.walk(folder_path, topdown=False))[:-1]:
        if get_folder_recursive_size(root) > max_size:
            print("Deleting " + root)
            shutil.rmtree(root, ignore_errors)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("max_allowed_size_in_mb", help="Max allowed size of a folder (MB)",
                        type=float)
    parser.add_argument("absolute_delete_path", help="Path where oversized directories should be deleted")

    parser.add_argument("-b", "--bottomup", action="store_true",
                        help="Make the script delete folders from bottom up (Delete deeper folders first)")
    args = parser.parse_args()

    # Convert size to bytes
    max_size_in_bytes = args.max_allowed_size_in_mb*1000000

    # Normalize inputted delete path
    delete_path = Path(args.absolute_delete_path)

    if(args.bottomup):
        delete_oversized_directories_bottom_up(delete_path, max_size_in_bytes)
    else:
        delete_oversized_directories_top_down(delete_path, max_size_in_bytes)

